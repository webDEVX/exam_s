#include <stdlib.h>
#include "dllist.h"

static struct bigint_dll* node_search_dll(struct bigint_dll* B, unsigned int i);
static struct bigint_dll* node_alloc_dll(digit_dll x);
static int node_create_dll(struct bigint_dll* B, digit_dll x);
static int node_delete_dll(struct bigint_dll* B);


bigint_dll* bigint_create_dll(void) {
	return NULL;
}

void bigint_delete_dll(bigint_dll **B) {

	if (B != NULL || *B != NULL) {
		bigint_delete_dll((*B)->next);
		free(*B);
		*B = NULL;
	}
}

unsigned int is_empty(bigint_dll **B) {
	return (B == NULL || *B == NULL);
}

unsigned int head_insert_dll(bigint_dll **B, digit_dll x) {
	
	if (B == NULL) {
		return 1;
	}
	else if (is_empty(B)) {
		return (*B = node_alloc_dll(x)) == NULL;
	}
	else {

		struct bigint_dll* tmp = node_alloc_dll(x);
		if (tmp != NULL) {
			tmp->next = (*B);
			(*B)->prev = tmp;
			*B = tmp;
		}
		return tmp == NULL;
	}
}

unsigned int tail_insert_dll(bigint_dll **B, digit_dll x) {

	if (B == NULL) {
		return 1;
	}
	else if (is_empty(B)) {
		return head_insert_dll(B,x);
	}
	else {

		struct bigint_dll* tmp = *B;
		while (tmp->next != NULL) tmp = tmp->next;

		return node_create_dll(tmp, x);
	}
}

unsigned int indx_insert_dll(bigint_dll** B, unsigned int i, digit_dll x) {

	if (B == NULL) {
		return 1;
	} 
	else if (i == 0) {
		return head_insert_dll(B, x);
	} 
	else {
		return node_create_dll(node_search_dll(*B, i - 1), x);
	}
}

unsigned int head_delete_dll(bigint_dll** B) {

	if (is_empty(B)) {
		return 1;
	}
	else {
	
		struct bigint_dll* tmp = *B;
		*B = tmp->next;
		free(tmp);
		return 0;
	}
}

unsigned int tail_delete_dll(bigint_dll**B) {

	if (is_empty(B)) {
		return 1;
	} 
	else if ((*B)->next == NULL) {
		return head_delete_dll(B);
	}
	else {
		struct bigint_dll* tmp = *B;
		while (tmp->next != NULL) tmp = tmp->next;
		return node_delete_dll(tmp);
	}
}

unsigned int indx_delete_dll(bigint_dll**B, unsigned int i) {

	if (is_empty(B)) {
		return 1;
	}
	else if (i == 0) {
		return head_delete_dll(B);
	}
	else {
		return node_delete_dll(node_search_dll(*B, i));
	}
}

digit_dll head_select_dll(bigint_dll **B) {

	if (is_empty(B)) {
		return '0';
	}
	else {
		return (*B)->x;
	}
}

digit_dll tail_select_dll(bigint_dll **B) {

	if (is_empty(B)) {
		return '0';
	} 
	else if ((*B)->next == NULL) {
		return head_select_dll(B);
	}
	else {	
		struct bigint_dll* tmp = *B;
		while (tmp->next != NULL) tmp = tmp->next;
		return tmp->x;
	}
}

digit_dll indx_select_dll(bigint_dll** B, unsigned int i) {

	if (is_empty(B)) {
		return '0';
	} 
	else if (i == 0) {
		return head_select_dll(B);
	}
	else {
		struct bigint_dll* tmp = node_search_dll(*B,i);
		return tmp == NULL ? '0' : tmp->x;
	}
}

static struct bigint_dll* node_search_dll(struct bigint_dll* B, unsigned int i) {

	if (B == NULL) {

		return NULL;
	}
	else {

		while (i-- > 0 && B != NULL) B = B->next;

		return B;
	}
}

static struct bigint_dll *node_alloc_dll(digit_dll x) {

	struct bigint_dll *tmp = (struct bigint_dll*) malloc(sizeof(struct bigint_dll));
		
	if (tmp != NULL) {
		tmp->x = x;
		tmp->prev = NULL;
		tmp->next = NULL;
	}

	return tmp;
}

static int node_create_dll(struct bigint_dll *B, digit_dll x) {
	if (B == NULL) {
		return 1;
	}
	else {
		
		struct bigint_dll* tmp = node_alloc_dll(x);

		if (tmp != NULL) {
			tmp->prev = B;
			tmp->next = B->next;
			if(B->next != NULL)
				B->next->prev = tmp;
			B->next = tmp;
		}
		return tmp == NULL;

	}
};

static int node_delete_dll(struct bigint_dll* B) {

	if (B == NULL) {
		return 1;
	}
	else {

		B->prev->next = B->next;
		if(B->next != NULL)
			B->next->prev = B->prev; 
		free(B);
		return 0; 
	}
}


