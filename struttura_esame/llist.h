#ifndef llist_h
#define llist_h

#include <stdio.h>

typedef signed char digit;

typedef struct bigint {
    digit x;
    struct bigint *next;
} bigint;

bigint *bigint_create_sll(void);

void bigint_delete_sll(bigint **);

int is_empty_sll(bigint **);

int is_inlist_sll(bigint **, digit);

int head_insert_sll(bigint **, digit);

int tail_insert_sll(bigint **, digit);

int indx_insert_sll(bigint **, int, digit);

int head_delete_sll(bigint **);

int tail_delete_sll(bigint **);

int indx_delete_sll(bigint **, int);

digit head_select_sll(bigint **);

digit tail_select_sll(bigint **);

digit indx_select_sll(bigint **, int);

int list_to_int_sll(bigint **);

int moltiplicate (bigint **, int);


#endif /* llist_h */
