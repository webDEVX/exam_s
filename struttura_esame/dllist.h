#ifndef dllist_h
#define dllist_h


typedef signed char digit_dll;

typedef struct bigint_dll {
	digit_dll x;
	struct bigint_dll* next;
	struct bigint_dll* prev;
} bigint_dll;


bigint_dll* bigint_create_dll(void);

void bigint_delete_dll(bigint_dll**);

unsigned int is_empty(bigint_dll**);

unsigned int head_insert_dll(bigint_dll**, digit_dll);

unsigned int tail_insert_dll(bigint_dll**, digit_dll);

unsigned int indx_insert_dll(bigint_dll**, unsigned int i, digit_dll);

unsigned int head_delete_dll(bigint_dll**);

unsigned int tail_delete_dll(bigint_dll**);

unsigned int indx_delete_dll(bigint_dll**, unsigned int i);

digit_dll head_select_dll(bigint_dll**);

digit_dll tail_select_dll(bigint_dll**);

digit_dll indx_select_dll(bigint_dll**, unsigned int);

#endif /* dllist_h */
