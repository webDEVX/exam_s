#include <stdio.h>
#include "dllist.h"

int main(int argc, const char * argv[]) {
  
	bigint_dll* bi = bigint_create_dll();
	
	head_insert_dll(&bi, '1');
	tail_insert_dll(&bi, '2');
	tail_insert_dll(&bi, '4');
	tail_insert_dll(&bi, '5');
	tail_insert_dll(&bi, '6');
	indx_insert_dll(&bi, 2, '3');

	printf("%c\n", indx_select_dll(&bi,6));
	
	
	/*
	bigint_dll* bi2 = bigint_create_dll();
	
	head_insert_dll(&bi2, '1');
	tail_insert_dll(&bi2, '2');
	tail_delete_dll(&bi2);
	*/

    return 0;
}
