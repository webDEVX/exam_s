#include <stdlib.h>
#include <math.h>

#include "llist.h"

static struct bigint *node_alloc_sll(digit);

static struct bigint *node_search_sll(bigint *, unsigned int);

static int node_delete_sll(bigint *);

static int node_insert_sll(bigint *, digit);

static int is_valid_digit_sll(digit);

static unsigned int list_length_sll(bigint **B);

static int m_list (bigint **, int);

void print_list_sll(struct bigint *);

bigint *bigint_create_sll(void) {
    return NULL;
}

void bigint_delete_sll(bigint **B) {
    
    if(B == NULL && *B == NULL)
        return ;
    
    bigint_delete_sll(&(*B)->next);
    free(B);
    *B = NULL;
}

int is_empty_sll(bigint **B) {
    return (B == NULL || *B == NULL);
}

int is_inlist_sll(bigint **B, digit x) {
   
    if(is_empty_sll(B)) return 0;
    
    struct bigint *tmp = *B;
    while (tmp != NULL || tmp->x == x)
        tmp = tmp->next;
   
    return tmp != NULL;
}

int head_insert_sll(bigint **B, digit x) {
    
    if(B == NULL || (!is_empty_sll(B) && x == 0)) return 1;
    
    
    struct bigint *tmp = node_alloc_sll(x);
    
    if(tmp != NULL) {
        tmp->next = *B;
        *B = tmp;
    }
    
    return tmp == NULL;
}

int tail_insert_sll(bigint **B, digit x) {

    if(B == NULL) return 1;
    
    if(is_empty_sll(B)) return head_insert_sll(B, x);
    
    struct bigint *tmp = *B;
    while (tmp->next != NULL) tmp = tmp->next;
    
    if(tmp == *B && tmp->x == 0) return 1;
    
    return node_insert_sll(tmp, x);
}

int indx_insert_sll(bigint **B, int i, digit x) {
    
    if(B == NULL)
        return 1;
    else if(i == 0)
        return head_insert_sll(B, x);
    else
        return node_insert_sll(node_search_sll(*B, i - 1), x);
}

int head_delete_sll(bigint **B) {
    
    if(B == NULL) return 1;
    
    struct bigint *tmp = *B;
    *B = tmp->next;
    free(tmp);
    
    return 0;
}

int tail_delete_sll(bigint **B) {
    
    if(B == NULL || is_empty_sll(B))
        return 1;
    else if ((*B)->next == NULL)
       return head_delete_sll(B);
    else {
        
        struct bigint *tmp = *B;
        while (tmp->next->next != NULL) tmp = tmp->next;
        
        return node_delete_sll(tmp);
    }
}

int indx_delete_sll(bigint **B, int i) {
    
    if(B == NULL)
        return 1;
    else if(i == 0)
        return head_delete_sll(B);
    else
        return node_delete_sll(node_search_sll(*B, i-1));
        
}

digit head_select_sll(bigint **B) {
    
    if (is_empty_sll(B)) return 0;
    return (*B)->x;
}

digit tail_select_sll(bigint **B) {
    
    if (is_empty_sll(B)) {
        return 0;
    } else if((*B)->next == NULL) {
        return head_select_sll(B);
    } else {
        struct bigint *tmp = *B;
        while (tmp->next != NULL) tmp = tmp->next;
        return tmp->x;
    }
}

digit indx_select_sll(bigint **B, int i) {
    
    if (is_empty_sll(B)) {
       return 0;
    } else if(i == 0) {
        return head_select_sll(B);
    } else {
        struct bigint *tmp = node_search_sll(*B, i);
        return tmp->x;
    }
}

int moltiplicate (bigint **B, int i) {
    
    int c = m_list(B, i);
    
    if (c > 0) return head_insert_sll(B, c);
    
    return 0;
    
}

int list_to_int_sll(bigint **B) {
    
    int res = 0;
    unsigned int ln = list_length_sll(B);
    struct bigint *tmp = *B;
    
    while (tmp != NULL) {
        res += tmp->x * pow(10, --ln);
        tmp = tmp->next;
    }
    
    return res;
}

static struct bigint *node_alloc_sll(digit x) {
    
    if(!is_valid_digit_sll(x)) return NULL;
    
    struct bigint *res = (struct bigint *) malloc(sizeof(bigint));
    
    if(res != NULL) {
        res->x = x;
        res->next = NULL;
    }
    
    return res;
}

static struct bigint *node_search_sll(bigint *B, unsigned int i) {
    
    if(B != NULL ) {
        struct bigint *tmp = B;
        while ( i-- > 0 && tmp->next != NULL) tmp = tmp->next;
        return tmp;
    }
    
    return NULL;
}

static int node_delete_sll(bigint *B) {
    
    if(B == NULL) return 1;
    
    struct bigint *tmp = B->next;
    B->next = tmp->next;
    free(tmp);
    
    return 0;
}

static int node_insert_sll(bigint *B, digit x) {
    
    struct bigint *tmp = node_alloc_sll(x);
    
    if(tmp != NULL) {
        tmp->next = B->next;
        B->next = tmp;
    }
    
    return tmp == NULL;
}

static unsigned int list_length_sll(bigint **B) {
   
    unsigned int c = 0;
    if(is_empty_sll(B)) return c;
    
    struct bigint *tmp = *B;
    
    while (tmp != NULL)  {
        tmp = tmp->next;
        c++;
    }
    
    return c;
}

static int is_valid_digit_sll(digit x) {
    return !(x < 0 || x > 9);
}

static int m_list (bigint **B, int i) {
    
    static int c = 0;
    struct bigint *tmp = *B;
    
    if(B != NULL && tmp != NULL) {
        m_list(&tmp->next, i);
        int r = (tmp->x * i) + c;
        if(r > 9) {
            c = r / 10;
            r = r % 10;
        } else {
            c = 0;
        }
        tmp->x = r;
    }
    
    return c;
}

void print_list_sll(bigint *B) {
    
    struct bigint *tmp = B;
    
    while (tmp != NULL) {
        printf("Digit: %d\n", tmp->x);
        tmp = tmp->next;
    }
}


