#include <stdlib.h>
#include "cllist.h"

#define C2D(x) ((x)-'0')

static struct bigint_cll *node_alloc_cll(digit_cll);

static struct bigint_cll *node_search_cll(struct bigint_cll *, unsigned int);

static int node_delete_cll(struct bigint_cll *);

static int node_insert_cll(struct bigint_cll *, digit_cll);

bigint_cll *bigint_create_cll(void) {
    return NULL;
}

void bigint_delete_cll(bigint_cll **B) {
    if(B == NULL || *B ==NULL) {
        
        struct bigint_cll *tmp = (*B)->next;
        
        while (tmp != *B) {
            node_delete_cll(tmp);
            tmp = tmp->next;
        }
        
        free(*B);
        *B = NULL;
    }
}

int is_empty_cll(bigint_cll **B) {
    return (B == NULL || *B ==NULL);
}

int is_inlist_cll(bigint_cll **B, digit_cll x) {
    
    if(is_empty_cll(B)) return 0;
    
    struct bigint_cll *tmp = *B;
    while (tmp != (*B)->prev && tmp->x != x) tmp = tmp->next;
    
    return tmp->x == x;
}

int head_insert_cll(bigint_cll **B, digit_cll x) {
    
    if(B == NULL) return 1;
    
    else if(is_empty_cll(B))
        return ((*B) = node_alloc_cll(x)) == NULL;
    
    else if (node_insert_cll((*B)->prev,x) == 1)
        return 1;
    else
        return (*B = (*B)->prev) == NULL;
}

int tail_insert_cll(bigint_cll **B, digit_cll x) {
    
    if(B == NULL) return 1;
    
    else if(is_empty_cll(B))
        return ((*B) = node_alloc_cll(x)) == NULL;
    else
        return node_insert_cll((*B)->prev,x) == 1;
    
}

int indx_insert_cll(bigint_cll **B, int i, digit_cll x) {
    if(i == 0) {
        return head_insert_cll(B, x);
    } else {
        return node_insert_cll(node_search_cll(*B, i-1), x);
    }
}

int head_delete_cll(bigint_cll **B) {
    
    if(is_empty_cll(B)) {
        return 1;
    } else if(*B == (*B)->next) {
        free(*B);
        *B = NULL;
        return 0;
    } else {
        struct bigint_cll *tmp = *B;
        *B = (*B)->next;
        return node_delete_cll(tmp);
    }
}

int tail_delete_cll(bigint_cll **B) {
   
    if(is_empty_cll(B)) {
        return 1;
    }
    else if(*B == (*B)->next) {
        return head_delete_cll(B);
    } else {
        return node_delete_cll((*B)->prev);
    }
}

int indx_delete_cll(bigint_cll **B, int i) {
    if(i == 0) {
        return head_delete_cll(B);
    } else {
        return node_delete_cll(node_search_cll(*B, i));
    }
}

digit_cll head_select_cll(bigint_cll **B) {
    
    if (is_empty_cll(B))
        return 0;
    
    return (*B)->x;
}

digit_cll tail_select_cll(bigint_cll **B) {
    
    if (is_empty_cll(B))
        return 0;
    
    return (*B)->prev->x;
}

digit_cll indx_select_cll(bigint_cll **B, int i) {
    
    if (is_empty_cll(B))
        return 0;
    
    struct bigint_cll *tmp = node_search_cll(*B, i);
    return tmp == NULL ? 0 : tmp->x;
}


bigint_cll* read(char *filename)
{
	struct bigint_cll* tmp = bigint_create_cll();
	FILE *f = fopen(filename, "r");
	
	int is_n = 0;

	if (f == NULL) return NULL;
	char c;
	while (fscanf(f, "%c", &c) >0 )
	{
		if (c == '-' || !(C2D(c) <=-1 || C2D(c) >= 10) ) {
			if (c == '-' && !is_n)
				is_n = 1;
			else if(c != '-') 
				tail_insert_cll(&tmp, C2D(c));
		}
	}

	fclose(f);
	if (is_n) {
		struct bigint_cll* head = node_search_cll(tmp, 0);
		head->x *= -1;
	}

	return tmp;
}

void* print(bigint_cll *B)
{
	if (B == NULL)
		return;
	
	printf("%d", B->x);

	struct bigint_cll* tmp = B->next;
	while (tmp != B)
	{
		printf("%d",tmp->x);
		tmp = tmp->next;
	}
}

static struct bigint_cll *node_alloc_cll(digit_cll x) {
    struct bigint_cll *tmp = (struct bigint_cll *) malloc(sizeof(struct bigint_cll));
    if(tmp != NULL) {
        tmp->x = x;
        tmp->next = tmp;
        tmp->prev = tmp;
    }
    return tmp;
}

static struct bigint_cll *node_search_cll(struct bigint_cll *B, unsigned int i) {
    if(B == NULL || i == 0) return B;
    
    struct bigint_cll *tmp = B->next;
    while (i-- > 0 && tmp != B) tmp = tmp->next;
    
    return tmp == B ? NULL : tmp;
}

static int node_delete_cll(struct bigint_cll *B) {
    
    if(B == NULL) return 1;
    
    B->prev->next = B->next;
    B->next->prev = B->prev;
    free(B);
    
    return 0;
}

static int node_insert_cll(struct bigint_cll *B, digit_cll x) {
    
    if(B == NULL) return 1;
    
    struct bigint_cll *tmp = node_alloc_cll(x);
    
    if(tmp != NULL) {
        tmp->next = B->next;
        tmp->prev = B;
        B->next = tmp;
        tmp->next->prev = tmp;
    }
    
    return tmp == NULL;
}

