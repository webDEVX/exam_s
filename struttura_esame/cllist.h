#ifndef cllist_h
#define cllist_h

#include <stdio.h>

typedef signed char digit_cll;

typedef struct bigint_cll {
    digit_cll x;
    struct bigint_cll *next;
    struct bigint_cll *prev;
} bigint_cll;

bigint_cll *bigint_create_cll(void);

void bigint_delete_cll(bigint_cll **);

int is_empty_cll(bigint_cll **);

int is_inlist_cll(bigint_cll **, digit_cll);

int head_insert_cll(bigint_cll **, digit_cll);

int tail_insert_cll(bigint_cll **, digit_cll);

int indx_insert_cll(bigint_cll **, int, digit_cll);

int head_delete_cll(bigint_cll **);

int tail_delete_cll(bigint_cll **);

int indx_delete_cll(bigint_cll **, int);

digit_cll head_select_cll(bigint_cll **);

digit_cll tail_select_cll(bigint_cll **);

digit_cll indx_select_cll(bigint_cll **, int);

// Dalla simulazione del 13/12/2019
bigint_cll* read(char *filename);

// Dalla simulazione del 13/12/2019
void* print(bigint_cll*);

#endif /* cllist_h */
